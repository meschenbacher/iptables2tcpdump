package main

import "fmt"
import "strings"
import "os"
import "io"
import "net"
import "flag"

import "log"
import iptp "github.com/kilo-io/iptables_parser"

type run struct {
	Filter string
	Pick   bool
}

type tcpdump struct {
	runs []run
}

func NewTcpdump(nets []string) *tcpdump {
	t := &tcpdump{runs: make([]run, 0)}
	if len(nets) > 0 {
		t.Add(fmt.Sprintf("(src net %s)", strings.Join(nets, " or ")), false)
	}
	return t
}

func (t *tcpdump) Add(filter string, pick bool) {
	t.runs = append(t.runs, run{Filter: filter, Pick: pick})
}

func (t *tcpdump) String() string {
	ret := ""
	for i, run := range t.runs {
		write := "-w -"
		read := "-r -"
		pipe := " | \\\n"
		if i == 0 {
			pipe = ""
			read = "-i any"
		}
		if i == len(t.runs)-1 {
			// do not print the last iteration of packets
			write = "-w /dev/null"
		}
		if run.Pick {
			pipe = " | \\\n tee >"
			ret = ret + fmt.Sprintf("%s( tcpdump --no-optimize -nn -U %s \"%s\" >/dev/stderr )", pipe, read, run.Filter)
			ret = ret + fmt.Sprintf(" | \\\n ( tcpdump --no-optimize -nn -U %s %s \"not (%s)\" )", read, write, run.Filter)
		} else {
			ret = ret + fmt.Sprintf("%s( tcpdump --no-optimize -nn -U %s %s \"%s\" )", pipe, read, write, run.Filter)
		}
	}
	return ret
}

type Main struct {
	t     *tcpdump
	rules []string
	pick  bool // state for picking rules between lines and identifies if this traffic is output
}

func NewMain(t *tcpdump) Main {
	return Main{t: t, rules: make([]string, 0)}
}

func (m *Main) handleRule(r iptp.Rule) {

	// We pick rules which have a drop
	if !m.pick && r.Jump.Name == "DROP" {
		// also emit tcpdump call
		m.t.Add("not "+strings.Join(m.rules, "\nand not "), m.pick)
		m.pick = true
		m.rules = nil
	} else if m.pick && r.Jump.Name == "ACCEPT" {
		m.t.Add(strings.Join(m.rules, "\nor "), m.pick)
		m.pick = false
		m.rules = nil
	}

	terms := make([]string, 0)
	if r.Destination != nil {
		if r.Destination.Not {
			log.Fatalf("negation not supported %v", r)
		}
		terms = append(terms, fmt.Sprintf("dst net %s", r.Destination.Value.String()))
	}
	if r.Source != nil {
		if r.Source.Not {
			log.Fatalf("negation not supported %v", r)
		}
		terms = append(terms, fmt.Sprintf("src net %s", r.Source.Value.String()))
	}
	for _, ma := range r.Matches {
		if ma.Type == "udp" || ma.Type == "tcp" || ma.Type == "multiport" {
			for k, v := range ma.Flags {
				if v.Not {
					log.Fatalf("negation not supported %v", r)
				}
				direction := ""
				if k == "destination-port" || k == "destination-ports" {
					// handling below
					direction = "dst"
				} else if k == "source-port" || k == "source-ports" {
					// handling below
					direction = "src"
				} else if k == "tcp-flags" {
					// IPv6 tcp[tcpflags] does not work
					// properly according to
					// pcap-filter(7), so check/work
					// around it
					tcpident := "ip and tcp[tcpflags]"
					if r.Destination != nil {
						if _, ipnet, err := net.ParseCIDR(r.Destination.Value.String()); err == nil && ipnet.IP.To4() == nil {
							tcpident = "ip6 and ip6[13+40]"
						}
					}
					if r.Source != nil {
						if _, ipnet, err := net.ParseCIDR(r.Source.Value.String()); err == nil && ipnet.IP.To4() == nil {
							tcpident = "ip6 and ip6[13+40]"
						}
					}
					if len(v.Values) == 2 && v.Values[0] == "ACK" && v.Values[1] == "ACK" {
						terms = append(terms, fmt.Sprintf("%s & (tcp-ack) != 0", tcpident))
						if r.Destination == nil && r.Source == nil {
							// no source and dest, add an
							// additional match for ipv6
							// TODO(meschenbacher) the src/dest
							// port(range) is not applied to these
							// rules right here
							tcpident = "ip6 and ip6[13+40]"
							m.rules = append(m.rules, fmt.Sprintf("(%s & (tcp-ack) != 0)", tcpident))
						}
					} else if len(v.Values) == 2 && v.Values[0] == "ACK,FIN,RST,SYN" && v.Values[1] == "RST" {
						terms = append(terms, fmt.Sprintf("%s & (tcp-ack|tcp-fin|tcp-rst|tcp-syn) == tcp-rst", tcpident))
						if r.Destination == nil && r.Source == nil {
							// no source and dest, add an
							// additional match for ipv6
							tcpident = "ip6 and ip6[13+40]"
							m.rules = append(m.rules, fmt.Sprintf("(%s & (tcp-ack|tcp-fin|tcp-rst|tcp-syn) == tcp-rst)", tcpident))
						}
					} else {
						log.Fatalf("unsupported tcp-flags %v %v", k, v)
					}
				} else if k == "icmp" || k == "icmp6" {
					// TODO(meschenbacher) implement
				} else {
					log.Fatalf("unsupported match %v %v", k, v)
				}

				// handle source and destination port(s)
				if direction == "dst" || direction == "src" {
					ports := make([]string, 0)
					for _, value := range v.Values {
						port := "port"
						if strings.Contains(value, ":") {
							parts := strings.Split(value, ":")
							port = "portrange"
							value = fmt.Sprintf("%s-%s", parts[0], parts[1])
						}
						ports = append(ports, fmt.Sprintf("%s %s %s", direction, port, value))
					}
					terms = append(terms, fmt.Sprintf("(%s)", strings.Join(ports, ") or (")))
				}
			}
		} else if ma.Type == "icmp" || ma.Type == "icmp6" {
			// TODO(meschenbacher) implement
		} else {
			log.Fatalf("unsupported match %s in %v", ma.Type, r)
		}
	}
	if len(terms) == 0 {
		return
	}
	m.rules = append(m.rules, fmt.Sprintf("(%s)", strings.Join(terms, " and ")))
}

func main() {
	strict := flag.Bool("strict", false, "Specify strict mode to error on iptables parsing errors.")
	flag.Parse()
	m := NewMain(NewTcpdump(flag.Args()))
	failureObserved := false
	bytes, err := io.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")

	for _, r := range lines {
		// iptp.NewFromString returns err == io.EOF if it parses an empty line but
		// also if it encounters e.g. a premature end of a rule so we catch that
		// before
		if len(r) == 0 {
			continue
		}
		tr, err := iptp.NewFromString(r)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error on line `%s': %v\n", r, err)
			failureObserved = true
			continue
		}
		switch r := tr.(type) {
		case iptp.Rule:
			m.handleRule(r)
			// case iptp.Policy:
			// // add a crafted rule
			// m.handleRule(iptp.Rule{
			// Chain: r.Chain,
			// // Source: &iptp.DNSOrIPPair{Value: net.IPNet{IP: "
			// Jump: &iptp.Target{Name: r.Action},
			// })
			// TODO(meschenbacher) handle default policy and let the user decide
			// if he wants to view the packets (default true).
		}
	}
	if failureObserved && *strict {
		log.Fatalf("Observed one or more parsing failures and strict mode was requested. Exiting abnormally.")
	}
	fmt.Printf("%s", m.t.String())
}
