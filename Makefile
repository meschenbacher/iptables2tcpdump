.PHONY: all clean

all: iptables2tcpdump

iptables2tcpdump: main.go
	go build

clean:
	rm -f iptables2tcpdump
