module gitlab.com/meschenbacher/iptables2tcpdump

go 1.21.6

replace github.com/kilo-io/iptables_parser => /home/max/git/iptables_parser

require (
	github.com/coreos/go-iptables v0.7.0 // indirect
	github.com/kilo-io/iptables_parser v0.0.0-20220520085151-275b7c93e8a7 // indirect
)
